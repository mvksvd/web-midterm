<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Купоны, скидки и акции в Казахстане: все скидочные купоны в одном месте на сайте Chocolife.me</title>
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1">  -->

	<!-- шрифт -->

	<!-- css -->
	<link rel="shortcut icon" href="images/favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.4.3/css/mdb.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/style2.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/search.css">
    <link rel="stylesheet" type="text/css" href="css/magicLine.css">
	<!-- javascript -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  	<script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;language=ru;key=AIzaSyAncU9HkJIztOt0XQoTk4Q954X775Blm10"></script>
    <script src="js/magicLine.js"></script>
  	

	
</head>
<body  id="page-top" class="index" data-spy="scroll" data-target=".navbar-fixed-top">
	<!-- Navbar -->
	<div class="menubar ">
	  <div class="containerbar row">
	    <ul class="list col-xs-9">
	      <li class="choco choco-item"><a href="" class="choco_link"></a></li>
	      <li class="mart mart-item"><a href="https://chocomart.kz/chocozhuma/?utm_source=chocolife.me&utm_medium=plashka&utm_campaign=chocozhuma" class="mart_link"></a></li>
	      <li class="travel travel-item"><a href="https://www.chocotravel.com/?utm_source=%D1%81hocolife.me&utm_medium=plashka" class="travel_link"></a></li>
	      <li class="lensmark lensmark-item"><a href="http://lensmark.kz/?utm_source=chocolife.me&utm_medium=plashka" class="lensmark_link"></a></li>
	      <li class="food food-item"><a href="http://chocofood.kz/?utm_source=chocolife.me&utm_medium=plashka" class="food_link"></a></li>
	      <li class="idoctor idoctor-item" ><a href="http://idoctor.kz/?utm_source=chocolife.me&utm_medium=plashka" class="idoctor_link"></a></li>
	    </ul>
	    <div class="usermenu col-xs-3" >
	      <div class="regismenu ">
	      		<span class="regis"><a href="">Регистрация</a></span>
	      		<span class="log"><a href="">Вход</a></span>
	      </div>
	      <div class="itemsbox">
	      	<a href="#">
	      		<div class="cart">
	      			<div class="cart-c">
	      				0
	      			</div>
	      		</div>
	      	</a>
	      </div>
	    </div>
	  </div>
	</div>
     


    <!-- Header -->
    <div class="clear"></div>  

    <div class="container" style="font-family: 'Roboto',sanf-serif;">
    		<div class="row">
    			<div class="citylist col-xs-3"><a href="">Алматы</a><span class="glyphicon glyphicon-triangle-bottom"></span></div>
    			<div class="faq col-xs-3 text-center" ><span class="faq-icon"></span><p style="margin: 10px auto;    letter-spacing: -0.6px;    font-size: 14px; font-weight: 400;">НУЖНА ПОМОЩЬ?</p></div>
    			<div class="prom col-xs-3 text-center"><span class="prom-icon"></span><p style="margin: 10px auto;    letter-spacing: -0.6px;    font-size: 14px; font-weight: 400;">ЗАЩИТА ПОКУПАТЕЛЕЙ</p></div>
    			<div class="feed col-xs-3 text-center"><span class="feed-icon"></span><p style="margin: 10px auto;    letter-spacing: -0.6px;    font-size: 14px; font-weight: 400;">ОБРАТНАЯ СВЯЗЬ</p></div>
    		</div>
    </div>

	<div class="clear2"></div>  

	<div class="container">
		<div class="row">
			<div class="choco-icon col-xs-3"><span class="choco-img"></span></div>
			<div class="mes col-xs-2"><p>Главное, чтобы Вы <br>были счастливы!</p></div>
			<div class="col-xs-7">
				<div class="flipkart-navbar-search smallsearch">
					<div class="row search">
	                    <input class="flipkart-navbar-input col-xs-8" type="" placeholder="Найти среди 621 акций" name="">
	                    <button class="flipkart-navbar-button col-xs-2">
	                        <span class="glyphicon glyphicon-search"></span>
	                    </button>
	                    
	                </div>
            	</div>
			</div>
		</div>
	</div>




	<!-- Banner -->
    <div class="container" style="padding: 10px 0 15px;"><img src="images/bording.jpg" alt=""></div>
    
    <!-- Sticky menu -->
    <div class="menu-wrap text-left" >
    	<div class="menu-wrap2 nav-wrap">
    			<ul class="content group col-xs-12" id="example-one">
    				<li class="all current_page_item"><a href="#" class="mirror">Все</a></li>
    				<li class="all"><a href="#" class="mirror">Новые</a></li>
    				<li class="all"><a href="#" class="mirror">Хиты продаж</a></li>
    				<li class="all"><a href="#" class="mirror">Развлечения и отдых</a></li>
    				<li class="all"><a href="#" class="mirror">Красота и здоровье</a></li>
    				<li class="all"><a href="#" class="mirror">Спорт</a></li>
    				<li class="all"><a href="#" class="mirror">Товары</a></li>
    				<li class="all"><a href="#" class="mirror">Услуги</a></li>
    				<li class="all"><a href="#" class="mirror">Еда</a></li>
    				<li class="all"><a href="#" class="mirror">Туризм,отели</a></li>
    			</ul>
    	</div>
    </div>

	<div class="container" style="padding: 25px 0 20px;">
    	<div class="row">
    			<div class="col-xs-3"><a href=""><img src="images/b1.jpg" alt=""></a></div>
    			<div class="col-xs-3"><a href=""><img src="images/b2.jpg" alt=""></a></div>
    			<div class="col-xs-3"><a href=""><img src="images/b3.jpg" alt=""></a></div>
    			<div class="col-xs-3"><a href=""><img src="images/b4.jpg" alt=""></a></div>
    	</div>
    </div>
	
	<div class="container" style="padding: 0 0 20px;">
		<div class="tablemenu">
			<div class="row text-left">
				<div class="col-xs-2"><p>Cортировать:</p></div>
				<div class="col-xs-2 point"><div class="rad"></div><p>популярные</p></div>
				<div class="col-xs-2 point"><div class="rad"></div><p>цена</p></div>
				<div class="col-xs-2 point"><div class="rad"></div><p>скидка</p></div>
				<div class="col-xs-2 point"><div class="rad"></div><p>новые</p></div>
				<div class="col-xs-2 point"><div class="rad"></div><p>рейтинг</p></div>
			</div>
		</div>
	</div>

	<!-- Boardings -->
	<div class="container" >
		<div class="row" >
			<div class="col-xs-4"><div class="cards"><a href=""><img src="images/1.jpg" alt="" class="hoverable"></a></div></div>
			<div class="col-xs-4"><div class="cards"><a href="tabagan"><img src="images/2.jpg" alt="" class="hoverable"></a></div></div>
			<div class="col-xs-4"><div class="cards"><a href=""><img src="images/3.jpg" alt="" class="hoverable"></a></div></div>
		</div>
		<div class="row">
			<div class="col-xs-4">
                    <div class="place">
                    	<div class="place-imgs">
                    		<a href=""><img src="images/7.jpg" alt="" class="place-img" ></a>
                    	</div>
                    	<div class="info">
                    		<div class="info-inner">
                    			<div class="inner-h">
                    				Отель Тау House            
                    			</div>
                    			<p class="inner-p">Проживание в лучших номерах на 1 сутки</p>
                    		</div>
                    	</div>
                    </div>
            
            </div>
            <div class="col-xs-4"><div class="cards"><a href=""><img src="images/4.jpg" alt="" class="hoverable"></a></div></div>
            <div class="col-xs-4">
                    <div class="place">
                    	<div class="place-imgs">
                    		<a href="cirk"><img src="images/cirk1.JPG" alt="" class="place-img"> </a>
                    	</div>
                    	<div class="infos">
                    		<div class="info-inner">
                    			<div class="inner-h">
                    				                Московский Цирк Карнелли!                      
                    			</div>
                    			<p class="inner-p">
                Цирк! Цирк! Цирк! Артисты «Большого Московского цирка» и «Цирка Никулина»           </p>
                    		</div>
                    	</div>
                    </div>
            
            </div>
			
		</div>
		<div class="row" >
			<div class="col-xs-4"><div class="cards"><a href=""><img src="images/5.jpg" alt="" class="hoverable"></a></div></div>
			<div class="col-xs-4">
                    <div class="place">
                    	<div class="place-imgs">
                    		<a href=""><img src="images/9.JPG" alt="" class="place-img"> </a>
                    	</div>
                    	<div class="info">
                    		<div class="info-inner">
                    			<div class="inner-h">
                    				Cinema Towers 3D          
                    			</div>
                    			<p class="inner-p">
                Идеальное сочетание: 2 билета на любой сеанс + попкорн + 2 колы!            </p>
                    		</div>
                    	</div>
                    </div>
            
            </div>
			<div class="col-xs-4"><div class="cards"><a href=""><img src="images/6.jpg" alt="" class="hoverable"></a></div></div>
		</div>

	</div>
	

	<div class="container-fluid" style="padding: 10px 0 25px">
		<div class="container seotext">
			<div class="row">
				<div class="col-xs-3">
					<img src="images/peoples.jpg" alt="" width="250" style="padding-top: 10px">
				</div>
				<div class="col-xs-9">
					<div class="inner-h2">
                    		ВСЕ СКИДКИ И АКЦИИ В ОДНОМ МЕСТЕ!          
                    </div>
                    <p class="inner-p2">Человеку для счастья нужно совсем мало: оставаться здоровым, иметь крепкую семью и хорошую работу, хранить гармонию души. Но иногда это не все. Не хватает какого-то приятного дополнения, например, получить бешеную скидку на популярную услугу. И этот бонус можем подарить вам именно мы! Благодаря купонам, приобретенным в нашем сервисе, вы сможете стать самым счастливым человеком в любой точке города Алматы.</p>
                    <p class="inner-p2">Мы предлагаем скидки в Алматы на самые разнообразные виды деятельности и услуги, которые распределены по таким рубрикам:</p>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<p class="inner-p2">-полная подборка (все самые актуальные);</p>
					<p class="inner-p2">-новые (самые свежие предложения);</p>
					<p class="inner-p2">-санатории (отдых и лечение в наиболее престижных санаториях);</p>
					<p class="inner-p2">-красота (услуги салонов красоты, а также отдельных мастеров);</p>
					<p class="inner-p2">-здоровье и спорт (услуги спортзалов, фитнес-клубов и т. д.);</p>
					<p class="inner-p2">-еда (рестораны, кондитерские, суши, доставка и т. д.);</p>
					<p class="inner-p2">-развлечения (караоке, сауна и т. д.);</p>
					<p class="inner-p2">-услуги (авто-услуги, свадебные услуги и т. д.);</p>
					<p class="inner-p2">-товары (для школы, для дома и т. д.);</p>
					<p class="inner-p2">-отдых (туризм, гостиницы, отели и т. д.).</p>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="inner-h2">
                    		ПОКУПАЙТЕ КУПОНЫ И ЭКОНОМЬТЕ НА УСЛУГАХ И ТОВАРАХ!         
                    </div>
                    <p class="inner-p2">В нашем купонном сервисе Шоколайф можно приобрести горящие скидки и купоны в Алматы с самым разным акционным диапазоном: от 30% до 90%. На сайте представлено более 530 разных возможностей для реализации личных планов. Например, вы можете в очередной раз посетить «Sarafan cafe», но теперь со скидочным купоном на 50% оставить там намного меньшую сумму. Такой купон обойдется вам всего в 399 тенге.</p>
                    <p class="inner-p2">Срок действия каждой акции в Алматы указан на нашем сайте. Эта информация поможет приобрести купон для посещения вами не только интересного предложения, но и оптимального во времени. Чтобы быстрее отыскать нужную акцию в нашем агрегаторе скидок, воспользуйтесь удобным поиском сайта онлайн по всем имеющимся рубрикам.</p>
                    <p class="inner-p2">Отныне и навсегда сайт акций и скидок в Алматы должен стать вашим лучшим другом и помощником! С нашей помощью вы сэкономите уйму денежных средств, невозвратимое время и дорогое здоровье. Удобное оформление сайта, правильно подобранная цветовая гамма, удобство в расположении рубрик не оставят равнодушным ни молодежь, ни людей преклонного возраста.</p>
                    <p class="inner-p2">Скидочные купоны в Алматы, представленные у нас — это гарантия настоящего качества. Мы ручаемся за каждого своего компаньона, уверены в безупречности каждого предложения, ведь все проверено лично нами! Если вы не успели применить скидку по назначению, или вас не устроило обслуживание — звоните по телефону, указанному на сайте, и наша служба заботы о пользователях обязательно даст вам все разъяснения. При возникновении вопросов или предложений также можно связаться с нами через форму обратной связи.</p>
				</div>
			</div>
		</div>
	</div>
	


	<div class="clear3"></div>
	<!-- footer -->
    <div class="container-fluid fluid2">
    	<div class="container">
    		<div class="row" >
    			<div class="col-xs-2">
    				<p class="inner-h3"> Компания </p>
                    <ul class="link-list">
                        <li> <a href="#"> О Chocolife.me </a> </li>
                        <li> <a href="#"> Пресса о нас</a> </li>
                        <li> <a href="#"> Контакты </a> </li>
                    </ul>
    			</div>
    			<div class="col-xs-2">
    				<p class="inner-h3"> Клиентам </p>
                    <ul class="link-list">
                        <li> <a href="#"> Обратная связь </a> </li>
                        <li> <a href="#"> Обучающий видеоролик</a> </li>
                        <li> <a href="#"> Вопросы и ответы </a> </li>
                        <li> <a href="#"> Публичная оферта </a> </li>
                    </ul>
    			</div>
    			<div class="col-xs-2">
    				<p class="inner-h3"> Партнерам </p>
                    <ul class="link-list">
                        <li> <a href="#"> Для Вашего Бизнеса </a> </li>
  
                    </ul>
    			</div>
    			<div class="col-xs-2"></div>
    			<div class="col-xs-4" style="padding-left: 50px;">
    				<p class="inner-h4"> Наше предложение </p>
                    <ul class="link-list" >
                        <li style="padding-bottom: 10px;"> Chocolife.me теперь еще удобнее и <br>всегда под рукой! <br>
  						<a href=""><img src="images/gplay.png" alt=""></a>
  						<a href=""><img src="images/app.png" alt=""></a>
  						</li>
                    </ul>
    			</div>
    		</div>
			<div class="clear4"></div>
    		<div class="row" style="padding-top: 15px">
    			<div class="col-xs-7">
    				<ul class="link-list">
                        <li> Chocolife.me | 2011-2018    -    Карта сайта</li>
  					
                    </ul>
    			</div>
    			<div class="col-xs-5 text-right" style="padding-left: 50px;">
    				<ul class="link-list">
                        <li> Chocolife.me в социальных сетях:
  						<a href=""><img src="images/vk.png" alt=""></a>
  						<a href=""><img src="images/face.png" alt=""></a>
  						<a href=""><img src="images/you.png" alt=""></a>
  						<a href=""><img src="images/insta.png" alt=""></a></li>
                    </ul>
    			</div>
    		</div>
    	</div>
    </div>  
	
	
</body>
</html>
