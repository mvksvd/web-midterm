<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Купоны, скидки и акции в Казахстане: все скидочные купоны в одном месте на сайте Chocolife.me</title>
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1">  -->

	<!-- шрифт -->

	<!-- css -->
	<link rel="shortcut icon" href="images/favicon.ico">
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
 	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.4.3/css/mdb.min.css">
  	<link rel="stylesheet" href="css/style.css">
  	<link rel="stylesheet" href="css/style2.css">
    <link rel="stylesheet" href="css/style3.css">
  	<link rel="stylesheet" href="css/search.css">
    <link rel="stylesheet" href="css/carousel.css">
    <link rel="stylesheet" type="text/css" href="css/magicLine.css">		

	<!-- javascript -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  	<script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;language=ru;key=AIzaSyAncU9HkJIztOt0XQoTk4Q954X775Blm10"></script>
    <script src="js/magicLine.js"></script>
  	

	
</head>
<body  id="page-top" class="index" data-spy="scroll" data-target=".navbar-fixed-top">
	<!-- Navbar -->
	<div class="menubar ">
	  <div class="containerbar row">
	    <ul class="list col-xs-9">
	      <li class="choco choco-item"><a href="index" class="choco_link"></a></li>
	      <li class="mart mart-item"><a href="https://chocomart.kz/chocozhuma/?utm_source=chocolife.me&utm_medium=plashka&utm_campaign=chocozhuma" class="mart_link"></a></li>
	      <li class="travel travel-item"><a href="https://www.chocotravel.com/?utm_source=%D1%81hocolife.me&utm_medium=plashka" class="travel_link"></a></li>
	      <li class="lensmark lensmark-item"><a href="http://lensmark.kz/?utm_source=chocolife.me&utm_medium=plashka" class="lensmark_link"></a></li>
	      <li class="food food-item"><a href="http://chocofood.kz/?utm_source=chocolife.me&utm_medium=plashka" class="food_link"></a></li>
	      <li class="idoctor idoctor-item" ><a href="http://idoctor.kz/?utm_source=chocolife.me&utm_medium=plashka" class="idoctor_link"></a></li>
	    </ul>
	    <div class="usermenu col-xs-3" >
	      <div class="regismenu ">
	      		<span class="regis"><a href="">Регистрация</a></span>
	      		<span class="log"><a href="">Вход</a></span>
	      </div>
	      <div class="itemsbox">
	      	<a href="#">
	      		<div class="cart">
	      			<div class="cart-c">
	      				0
	      			</div>
	      		</div>
	      	</a>
	      </div>
	    </div>
	  </div>
	</div>
     


    <!-- Header -->
    <div class="clear"></div>  

    <div class="container" style="font-family: 'Roboto',sanf-serif;">
    		<div class="row">
    			<div class="citylist col-xs-3"><a href="">Алматы</a><span class="glyphicon glyphicon-triangle-bottom"></span></div>
    			<div class="faq col-xs-3 text-center" ><span class="faq-icon"></span><p style="margin: 10px auto;    letter-spacing: -0.6px;    font-size: 14px; font-weight: 400;">НУЖНА ПОМОЩЬ?</p></div>
    			<div class="prom col-xs-3 text-center"><span class="prom-icon"></span><p style="margin: 10px auto;    letter-spacing: -0.6px;    font-size: 14px; font-weight: 400;">ЗАЩИТА ПОКУПАТЕЛЕЙ</p></div>
    			<div class="feed col-xs-3 text-center"><span class="feed-icon"></span><p style="margin: 10px auto;    letter-spacing: -0.6px;    font-size: 14px; font-weight: 400;">ОБРАТНАЯ СВЯЗЬ</p></div>
    		</div>
    </div>

	<div class="clear2"></div>  

	<div class="container">
		<div class="row">
			<div class="choco-icon col-xs-3"><span class="choco-img"></span></div>
			<div class="mes col-xs-2"><p>Главное, чтобы Вы <br>были счастливы!</p></div>
			<div class="col-xs-7">
				<div class="flipkart-navbar-search smallsearch">
					<div class="row search">
	                    <input class="flipkart-navbar-input col-xs-8" type="" placeholder="Найти среди 621 акций" name="">
	                    <button class="flipkart-navbar-button col-xs-2">
	                        <span class="glyphicon glyphicon-search"></span>
	                    </button>
	                    
	                </div>
            	</div>
			</div>
		</div>
	</div>




	<!-- Banner -->

    <div class="clear2"></div>
    <!-- Sticky menu -->
    <div class="menu-wrap text-left">
        <div class="menu-wrap2 nav-wrap">
                <ul class="content group col-xs-12" id="example-one">
                    <li class="all current_page_item"><a href="#" class="mirror">Все</a></li>
                    <li class="all"><a href="#" class="mirror">Новые</a></li>
                    <li class="all"><a href="#" class="mirror">Хиты продаж</a></li>
                    <li class="all"><a href="#" class="mirror">Развлечения и отдых</a></li>
                    <li class="all"><a href="#" class="mirror">Красота и здоровье</a></li>
                    <li class="all"><a href="#" class="mirror">Спорт</a></li>
                    <li class="all"><a href="#" class="mirror">Товары</a></li>
                    <li class="all"><a href="#" class="mirror">Услуги</a></li>
                    <li class="all"><a href="#" class="mirror">Еда</a></li>
                    <li class="all"><a href="#" class="mirror">Туризм,отели</a></li>
                </ul>
        </div>
    </div>


    <!-- 	Company info -->
    
    <div class="container">
        <div class="company">
            <div class="row top-line">
                <div class="col-xs-6"><span class="top-text">Можно купить с 25 января по 31 января</span></div>
                <div class="col-xs-6 text-right"><span class="top-text">Можно воспользоваться до 4 февраля 2018 года</span></div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="inner-h1">Цирк! Цирк! Цирк! Артисты «Большого Московского цирка» и «Цирка Никулина» с новой зажигательной программой ждут вас 4 февраля! Билеты со скидкой 30%!</h1>
                </div>
            </div>
            <div class="row">
                <!-- CAROUSEL -->
                <div class="col-xs-9">
                    <section class="section-white">
                      <div class="container-carousel">

                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                          <!-- Indicators -->
                          <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                          </ol>

                          <!-- Wrapper for slides -->
                          <div class="carousel-inner">
                            <div class="item active">
                              <img src="images/cirk1.JPG" alt="...">

                            </div>
                            <div class="item">
                              <img src="images/cirk2.JPG" alt="...">

                            </div>

                          </div>

                          <!-- Controls -->
                          <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                          </a>
                          <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                          </a>
                        </div>

                      </div>
                    </section>
                </div>
                <!-- CAROUSEL -->

                <div class="col-xs-3" style="padding-top: 40px;">
                    <p class="price">от 1400 тг.</p>
                    <p class="price-2">экономия от <strong> 600 тг.</strong></p>
                    <span class="buy"><a href="">Провести снова</a></span>
                    <p class="price-2" style="padding-left: 25px;">купили<strong> 278 человек</strong></p>
                    <div class="hr">
                        <span class="glyphicon glyphicon-time" style="padding: 15px;"><p class="price-3" style="padding-left: 5px; font-weight: 500;font-size: 19px;display: inline;">Акция завершена</p></span>
                    </div>
                    
                        <ul class="links">
                            <li>
                            <a href=""><img src="images/vk.png" alt=""></a>
                            <a href=""><img src="images/face.png" alt=""></a>
                            <a href=""><img src="images/you.png" alt=""></a>
                            <a href=""><img src="images/insta.png" alt=""></a></li>
                        </ul>
                   
            
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="company">
            <div class="row top-top-line">
                <div class="col-xs-2 btn-checked"><a href="">Информация</a></div>
                <div class="col-xs-2 oth"><a href="">Отзывы(39)</a></div>
                <div class="col-xs-2 oth"><a href="">Вопросы(153)</a></div>
                <div class="col-xs-3 oth"><a href="">Получить 5000 тенге</a></div>
                <div class="col-xs-3 ques"><a href="">Как воспользоваться акцией?</a></div>

            </div>
            <div class="row">
                <!-- 1 колонна -->
                <div class="col-xs-5">
                    <div class="row def">
                        <div class=" col-xs-2 def-img"></div>
                        <p class=" def-txt" s>
                            Расширенная защита покупателей
                        </p>


                        <div class="infotext" style="padding-top: 10px;">
                        <p class="head-p">Условия:</p>
                            <ul>
                                <li>Сертификат предоставляет возможность посетить Казахский Государственный Цирк.</li>
                                
                                        <li>Выбор мест не предоставляется.</li>
                                        <li>Билеты выдаются в порядке очередности.</li>
                                        <li><strong>Дата и время проведения:</strong> 4 февраля, в 16:00.</li>
                                        <li><strong>Адрес проведения:</strong> г. Алматы, пр. Абая, 50, &laquo;Казахский Государственный Цирк&raquo;.<br />
&nbsp;<br />
<strong>Сертификат необходимо обменять на билеты в офисе Сhocolife.me</strong>&nbsp;<strong>до 3 февраля (включительно)</strong> по адресу: г. Алматы, ул. Байзакова, 280, БЦ Almaty Towers, офис 222.</li>
                                        <li>Скидка не суммируется с другими действующими предложениями цирка.</li>
                                        <li>Дети до 4 лет могут посещать цирк бесплатно (без предоставления места).</li>
                                <li>Ограничений по возрасту нет.</li>
                                <p>Согласно внутренним правилам цирка, приносить с собой еду не разрешается.</p>
                                <li>После приобретения сертификата дополнительно оплачивать ничего не нужно.</li>
                                <li><strong>Справки по телефону:</strong><br />
+7 (727) 346-85-88&nbsp;(офис Chocolife.me).</li>
                                <li>Вы можете приобрести неограниченное количество сертификатов по данной акции как для себя, так и в подарок.</li>
                                <li><strong>Сертификат распечатывать необязательно, достаточно сообщить его номер и SC-код.</strong></li>
                                <li>Сертификат действителен до 28 февраля 2018 г. (включительно).</li>
                                <li><a href=""></a>Политика по возврату средств</li>
                                <li><a href="">Стандартные условия каждой акции</a></li>

                            </ul>
                            <p class="head-p">Адрес</p>
                            <ul>
                                <li>г. Алматы, ул. Байзакова, 280 (уг. ул. Сатпаева), БЦ Алматы Тауэрс, 2 этаж, кабинет 222 (офис Chocolife.me) <a href="Посмотреть на карте"></a></li>
                                <li>Телефон                                    <br>
                                                                            +7 (727) 346-85-88  </li>
                                <li>График&nbsp;работы:<br/>
                                        Ежедневно: c 09:00 до 22:00                                                              </li>
                            </ul>
                        </div>
                    </div>
                    

                </div>
                <!-- 2 колонаа -->
                <div class="col-xs-7" style="padding-left: 50px;">
                    <div class="map">
                        <a class="dg-widget-link" href="http://2gis.kz/almaty/firm/9429940001186374/center/76.91561579704285,43.23723852471494/zoom/18?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Алматы</a><div class="dg-widget-link"><a href="http://2gis.kz/almaty/firm/9429940001186374/photos/9429940001186374/center/76.91561579704285,43.23723852471494/zoom/17?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=photos">Фотографии компании</a></div><div class="dg-widget-link"><a href="http://2gis.kz/almaty/center/76.91589,43.237504/zoom/18/routeTab/rsType/bus/to/76.91589,43.237504╎Chocolife.me, сервис коллективных покупок?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до Chocolife.me, сервис коллективных покупок</a></div><script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"width":440,"height":300,"borderColor":"#a3a3a3","pos":{"lat":43.23723852471494,"lon":76.91561579704285,"zoom":18},"opt":{"city":"almaty"},"org":[{"id":"9429940001186374"}]});</script><noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>
                    </div>
                    <div class="video">
                        <iframe src="https://www.youtube.com/embed/p8W8WsCm-gU" frameborder="0" width="500" height="300"></iframe>
                    </div>
                    <div class="infotext">
                        <p class="head-p">Особенности:</p>
                        <ul>
                            <li>Артисты «Большого Московского цирка» и «Цирка Никулина» лауреаты и призеры различных мировых фестивалей циркового искусство представляют:</li>
                            <ul>
                                <ul>
                                    <li>акробаты на подкидной доске;</li>
                                    <li>воздушные гимнасты;</li>
                                    <li>лапундеры (обезьяны);</li>
                                    <li>нубийские козлики;</li>
                                    <li>мини лошадка;</li>
                                    <li>конное трио (высшая школа верховой езды);</li>
                                    <li>собаки;</li>
                                    <li>и конечно же клоуны.</li>
                                </ul>
                            </ul>
                            <li>Длительность — 2,5 часа.</li>
                            <li>Артисты:</li>
                            <ul>
                                <ul>
                                    <li>Ковгар Андрей;</li>
                                    <li>Кох-Кукис Мария;</li>
                                    <li>Гукаев Константин.</li>
                                </ul>
                            </ul>
                            
                            

                        </ul>
                    </div>
                </div>

            </div>
            <div class="row bot-line">
                <div class="col-xs-2"><p class="top-text2">Способы оплаты:</p></div>
                <div class="col-xs-3"><span style="position: relative;bottom: 10px;right: 50px"><img src="images/visa.png" alt=""></span></div>
                <div class="col-xs-4"></div>
                <div class="col-xs-3"><a href="{{route('crud.index')}}" class="buy">Есть вопросы?</a></div>
                
            </div>
        </div>
    </div>


	


	<div class="clear3"></div>
	<!-- footer -->
    <div class="container-fluid fluid2">
    	<div class="container">
    		<div class="row" >
    			<div class="col-xs-2">
    				<p class="inner-h3"> Компания </p>
                    <ul class="link-list">
                        <li> <a href="#"> О Chocolife.me </a> </li>
                        <li> <a href="#"> Пресса о нас</a> </li>
                        <li> <a href="#"> Контакты </a> </li>
                    </ul>
    			</div>
    			<div class="col-xs-2">
    				<p class="inner-h3"> Клиентам </p>
                    <ul class="link-list">
                        <li> <a href="#"> Обратная связь </a> </li>
                        <li> <a href="#"> Обучающий видеоролик</a> </li>
                        <li> <a href="#"> Вопросы и ответы </a> </li>
                        <li> <a href="#"> Публичная оферта </a> </li>
                    </ul>
    			</div>
    			<div class="col-xs-2">
    				<p class="inner-h3"> Партнерам </p>
                    <ul class="link-list">
                        <li> <a href="#"> Для Вашего Бизнеса </a> </li>
  
                    </ul>
    			</div>
    			<div class="col-xs-2"></div>
    			<div class="col-xs-4" style="padding-left: 50px;">
    				<p class="inner-h4"> Наше предложение </p>
                    <ul class="link-list" >
                        <li style="padding-bottom: 10px;"> Chocolife.me теперь еще удобнее и <br>всегда под рукой! <br>
  						<a href=""><img src="images/gplay.png" alt=""></a>
  						<a href=""><img src="images/app.png" alt=""></a>
  						</li>
                    </ul>
    			</div>
    		</div>
			<div class="clear4"></div>
    		<div class="row" style="padding-top: 15px">
    			<div class="col-xs-7">
    				<ul class="link-list">
                        <li> Chocolife.me | 2011-2018    -    Карта сайта</li>
  					
                    </ul>
    			</div>
    			<div class="col-xs-5 text-right" style="padding-left: 50px;">
    				<ul class="link-list">
                        <li> Chocolife.me в социальных сетях:
  						<a href=""><img src="images/vk.png" alt=""></a>
  						<a href=""><img src="images/face.png" alt=""></a>
  						<a href=""><img src="images/you.png" alt=""></a>
  						<a href=""><img src="images/insta.png" alt=""></a></li>
                    </ul>
    			</div>
    		</div>
    	</div>
    </div>  
	
	
</body>
</html>