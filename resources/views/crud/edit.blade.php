@extends('layout')


    
@section('content')

@include('errors')

        <div class="container">
            <h3>Edittask # - {{$myTask->id}}</h3>
            
            <div class="row">
                <div class="col-md-12">
                    
                {!! Form::open(['route' => ['crud.update', $myTask -> id]]) !!}
                    <div class="form-group">
                        <input type="text" class="form-control" name="title" value="{{$myTask->title}}">  
                        <br>          
                        <textarea name="description" id="1" cols="30" rows="10" class="form-control" value="">{{$myTask->description}}</textarea>
                        <br>
                        <button type="submit" class="btn btn-warning">Submit</button>
                    </div>
                {!! Form::close() !!}
                </div>  
            </div>
        </div>

@endsection