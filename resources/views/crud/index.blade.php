@extends('layout')


    
@section('content')
        <div class="container">
            
            <h3>Часто задаваемые впоросы</h3>
            <a href="{{route('crud.create')}}" class="btn btn-success">Оставьте свой вопрос</a>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Title</td>

                                <td>Actions</td>

                            </tr>
                        </thead>
                    @foreach($quest as $task)
                    <tbody>
                        <tr>
                            <td>{{ $task->id }}</td>
                            <td>{{ $task->title }}</td>
                            
                            <td>
                                
                                <a href="{{route('crud.show', $task->id)}}" class="" >
                                    <i class="glyphicon glyphicon-eye-open"></i>  
                                </a>
                            
                            </td>        
                        </tr>
                    </tbody>  
                  @endforeach                 
                </table>
                {!! $quest->render() !!}
                </div>   
            </div>

            
        </div>

@endsection