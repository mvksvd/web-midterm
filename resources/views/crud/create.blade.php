@extends('layout')


    
@section('content')

@include('errors')

        <div class="container">
            <h3>Форма заполнения вопроса</h3>
            
            <div class="row">
                <div class="col-md-12">
                    
                {!! Form::open(['route' => ['crud.store']]) !!}
                    <div class="form-group">
                        <h3>Тема:</h3>
                        <input type="text" class="form-control" name="title" value="{{old('title')}}">  
                        <br>
                        <h3>Описание:</h3>          
                        <textarea name="description" id="" cols="30" rows="10" class="form-control" value="{{old('description')}}"></textarea>
                        <br>
                        <button class="btn btn-success">Отправить</button>
                    </div>
                {!! Form::close() !!}
                
                </div>  
            </div>
        </div>

@endsection