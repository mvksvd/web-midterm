<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Купоны, скидки и акции в Казахстане: все скидочные купоны в одном месте на сайте Chocolife.me</title>
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1">  -->

	<!-- шрифт -->

	<!-- css -->
	<link rel="shortcut icon" href="images/favicon.ico">
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
 	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.4.3/css/mdb.min.css">
  	<link rel="stylesheet" href="css/style.css">
  	<link rel="stylesheet" href="css/style2.css">
    <link rel="stylesheet" href="css/style3.css">
  	<link rel="stylesheet" href="css/search.css">
    <link rel="stylesheet" href="css/carousel.css">
    <link rel="stylesheet" type="text/css" href="css/magicLine.css">		

	<!-- javascript -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  	<script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;language=ru;key=AIzaSyAncU9HkJIztOt0XQoTk4Q954X775Blm10"></script>
    <script src="js/magicLine.js"></script>
  	

	
</head>
<body  id="page-top" class="index" data-spy="scroll" data-target=".navbar-fixed-top">
	<!-- Navbar -->
	<div class="menubar ">
	  <div class="containerbar row">
	    <ul class="list col-xs-9">
	      <li class="choco choco-item"><a href="" class="choco_link"></a></li>
	      <li class="mart mart-item"><a href="https://chocomart.kz/chocozhuma/?utm_source=chocolife.me&utm_medium=plashka&utm_campaign=chocozhuma" class="mart_link"></a></li>
	      <li class="travel travel-item"><a href="https://www.chocotravel.com/?utm_source=%D1%81hocolife.me&utm_medium=plashka" class="travel_link"></a></li>
	      <li class="lensmark lensmark-item"><a href="http://lensmark.kz/?utm_source=chocolife.me&utm_medium=plashka" class="lensmark_link"></a></li>
	      <li class="food food-item"><a href="http://chocofood.kz/?utm_source=chocolife.me&utm_medium=plashka" class="food_link"></a></li>
	      <li class="idoctor idoctor-item" ><a href="http://idoctor.kz/?utm_source=chocolife.me&utm_medium=plashka" class="idoctor_link"></a></li>
	    </ul>
	    <div class="usermenu col-xs-3" >
	      <div class="regismenu ">
	      		<span class="regis"><a href="">Регистрация</a></span>
	      		<span class="log"><a href="">Вход</a></span>
	      </div>
	      <div class="itemsbox">
	      	<a href="#">
	      		<div class="cart">
	      			<div class="cart-c">
	      				0
	      			</div>
	      		</div>
	      	</a>
	      </div>
	    </div>
	  </div>
	</div>
     


    <!-- Header -->
    <div class="clear"></div>  

    <div class="container" style="font-family: 'Roboto',sanf-serif;">
    		<div class="row">
    			<div class="citylist col-xs-3"><a href="">Алматы</a><span class="glyphicon glyphicon-triangle-bottom"></span></div>
    			<div class="faq col-xs-3 text-center" ><span class="faq-icon"></span><p style="margin: 10px auto;    letter-spacing: -0.6px;    font-size: 14px; font-weight: 400;">НУЖНА ПОМОЩЬ?</p></div>
    			<div class="prom col-xs-3 text-center"><span class="prom-icon"></span><p style="margin: 10px auto;    letter-spacing: -0.6px;    font-size: 14px; font-weight: 400;">ЗАЩИТА ПОКУПАТЕЛЕЙ</p></div>
    			<div class="feed col-xs-3 text-center"><span class="feed-icon"></span><p style="margin: 10px auto;    letter-spacing: -0.6px;    font-size: 14px; font-weight: 400;">ОБРАТНАЯ СВЯЗЬ</p></div>
    		</div>
    </div>

	<div class="clear2"></div>  

	<div class="container">
		<div class="row">
			<div class="choco-icon col-xs-3"><span class="choco-img"></span></div>
			<div class="mes col-xs-2"><p>Главное, чтобы Вы <br>были счастливы!</p></div>
			<div class="col-xs-7">
				<div class="flipkart-navbar-search smallsearch">
					<div class="row search">
	                    <input class="flipkart-navbar-input col-xs-8" type="" placeholder="Найти среди 621 акций" name="">
	                    <button class="flipkart-navbar-button col-xs-2">
	                        <span class="glyphicon glyphicon-search"></span>
	                    </button>
	                    
	                </div>
            	</div>
			</div>
		</div>
	</div>




	<!-- Banner -->

    <div class="clear2"></div>
    <!-- Sticky menu -->
    <div class="menu-wrap text-left">
        <div class="menu-wrap2 nav-wrap">
                <ul class="content group col-xs-12" id="example-one">
                    <li class="all current_page_item"><a href="#" class="mirror">Все</a></li>
                    <li class="all"><a href="#" class="mirror">Новые</a></li>
                    <li class="all"><a href="#" class="mirror">Хиты продаж</a></li>
                    <li class="all"><a href="#" class="mirror">Развлечения и отдых</a></li>
                    <li class="all"><a href="#" class="mirror">Красота и здоровье</a></li>
                    <li class="all"><a href="#" class="mirror">Спорт</a></li>
                    <li class="all"><a href="#" class="mirror">Товары</a></li>
                    <li class="all"><a href="#" class="mirror">Услуги</a></li>
                    <li class="all"><a href="#" class="mirror">Еда</a></li>
                    <li class="all"><a href="#" class="mirror">Туризм,отели</a></li>
                </ul>
        </div>
    </div>


    <!-- 	Company info -->
    
    <div class="container">
        <div class="company">
            <div class="row top-line">
                <div class="col-xs-6"><span class="top-text">Можно купить с 22 декабря по 4 февраля</span></div>
                <div class="col-xs-6 text-right"><span class="top-text">Можно воспользоваться до 28 февраля 2018 года</span></div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="inner-h1">Экстремально круто! Абсолютно новый снежный склон ждет вас! Катания на лыжах и сноуборде для взрослых и детей со скидкой до 30% в СРК Табаган!</h1>
                </div>
            </div>
            <div class="row">
                <!-- CAROUSEL -->
                <div class="col-xs-9">
                    <section class="section-white">
                      <div class="container-carousel">

                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                          <!-- Indicators -->
                          <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                          </ol>

                          <!-- Wrapper for slides -->
                          <div class="carousel-inner">
                            <div class="item active">
                              <img src="images/c1.jpg" alt="...">

                            </div>
                            <div class="item">
                              <img src="images/c2.jpg" alt="...">

                            </div>
                            <div class="item">
                              <img src="images/c3.jpg" alt="...">

                            </div>
                          </div>

                          <!-- Controls -->
                          <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                          </a>
                          <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                          </a>
                        </div>

                      </div>
                    </section>
                </div>
                <!-- CAROUSEL -->

                <div class="col-xs-3" style="padding-top: 40px;">
                    <p class="price">от 1400 тг.</p>
                    <p class="price-2">экономия от <strong> 600 тг.</strong></p>
                    <span class="buy"><a href="">Купить</a></span>
                    <p class="price-2" style="padding-left: 25px;">купили<strong> 2080 человек</strong></p>
                    <div class="hr">
                        <p class="price-2" style="padding-left: 10px">До завершения акции осталось:</p>
                        <span class="glyphicon glyphicon-time" style="padding-left: 50px"><p class="price-3" style="font-weight: 500;font-size: 19px;display: inline;"> 06:26:42</p></span>
                    </div>
                    
                        <ul class="links">
                            <li>
                            <a href=""><img src="images/vk.png" alt=""></a>
                            <a href=""><img src="images/face.png" alt=""></a>
                            <a href=""><img src="images/you.png" alt=""></a>
                            <a href=""><img src="images/insta.png" alt=""></a></li>
                        </ul>
                   
            
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="company">
            <div class="row top-top-line">
                <div class="col-xs-2 btn-checked"><a href="">Информация</a></div>
                <div class="col-xs-2 oth"><a href="">Отзывы(39)</a></div>
                <div class="col-xs-2 oth"><a href="">Вопросы(153)</a></div>
                <div class="col-xs-3 oth"><a href="">Получить 5000 тенге</a></div>
                <div class="col-xs-3 ques"><a href="">Как воспользоваться акцией?</a></div>

            </div>
            <div class="row">
                <!-- 1 колонна -->
                <div class="col-xs-5">
                    <div class="row def">
                        <div class=" col-xs-2 def-img"></div>
                        <p class=" def-txt" s>
                            Расширенная защита покупателей
                        </p>


                        <div class="infotext" style="padding-top: 10px;">
                        <p class="head-p">Условия:</p>
                            <ul>
                                <li>Сертификат предоставляет возможность посетить спортивно-развлекательный комплекс Табаган.</li>
                                <p><strong>Виды сертификатов:</strong></p>
                                <ul>
                                    <ul>
                                        <li>Дневной абонемент на взрослого (в выходные) -
                        4 500 тг.                         вместо 6 000 тг.      <a href="" target="">купить</a></li>
                                        <li>Дневной абонемент на взрослого (в будни) — 3 000 тг. вместо 4 000 тг.<a href="" target="">купить</a></li>
                                        <li>Дневной абонемент детский (в выходные) — 2 100 тг. вместо 3 000 тг.<a href="" target="">купить</a></li>
                                        <li>Дневной абонемент детский (в будни) — 1 500 тг. вместо 2 000 тг.<a href="" target="">купить</a></li>
                                        <li>Ночной абонемент на взрослого (в выходные) — 2 300 тг. вместо 3 000 тг.<a href="" target="">купить</a></li>
                                        <li>Ночной абонемент на взрослого (в будни) — 1 400 тг. вместо 2 000 тг.<a href="" target="">купить</a></li>
                                    </ul>
                                </ul>
                                <li><strong>Купленный сертификат действует только на один склон - на новый или на старый!</strong></li>
                                <li>После приобретения сертификата дополнительно оплачивается аренда инвентаря. С ценами можете ознакомиться в <a href=""> прайс-листе.</a></li>
                                <li>Детям с 11 лет необходимо приобретать взрослые абонементы.</li>
                                <li>Перед тем, как получить услугу, обязательно сообщайте нашему партнеру о том, что Вы обратились по акции Chocolife.me.</li>
                                <li><strong>Справки по телефонам:</strong><br />
+7 (775) 666-64-01 (звонки принимаются с 9 до 18:00 в будни),<br />
+7 (700) 978-54-91,<br />
+7 (707) 712-01-07.</li>
                                <li>Вы можете приобрести неограниченное количество сертификатов по данной акции как для себя, так и в подарок.</li>
                                <li><strong>Сертификат распечатывать необязательно, достаточно сообщить его номер и SC-код.</strong></li>
                                <li>Сертификат действителен до 28 февраля 2018 г. (включительно).</li>
                                <li><a href=""></a>Политика по возврату средств</li>
                                <li><a href="">Стандартные условия каждой акции</a></li>

                            </ul>
                            <p class="head-p">Адрес</p>
                            <ul>
                                <li>Алматинская обл., Талгарский район, садоводческое общество, ул. Мичурина, 4, «Бескайнар», база отдыха «Табаган» <a href="Посмотреть на карте"></a></li>
                                <li>График&nbsp;работы:<br/>
                                        Ежедневно: c 09:00 до 22:00                                                              </li>
                            </ul>
                        </div>
                    </div>
                    

                </div>
                <!-- 2 колонаа -->
                <div class="col-xs-7" style="padding-left: 40px;">
                    <div class="map">
                        <a class="dg-widget-link" href="http://2gis.kz/almaty/firm/9429940000896618/center/77.09661662578584,43.21476277783496/zoom/18?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Алматы</a><div class="dg-widget-link"><a href="http://2gis.kz/almaty/center/77.096622,43.214952/zoom/18/routeTab/rsType/bus/to/77.096622,43.214952╎TABAGAN Mountain Resort, горный курорт?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до TABAGAN Mountain Resort, горный курорт</a></div><script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"width":440,"height":300,"borderColor":"#a3a3a3","pos":{"lat":43.21476277783496,"lon":77.09661662578584,"zoom":18},"opt":{"city":"almaty"},"org":[{"id":"9429940000896618"}]});</script><noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>
                    </div>
                    <div class="video">
                        <iframe src="https://www.youtube.com/embed/CIMOwbK8axo" frameborder="0" width="500" height="300"></iframe>
                    </div>
                    <div class="infotext">
                        <p class="head-p">Особенности:</p>
                        <ul>
                            <li>Спортивно-развлекательный комплекс &laquo;<strong>Табаган</strong>&raquo; приглашает вас провести незабываемый зимний отдых с друзьями и близкими.<br />
Готовы к приключениям? Здесь Вас ждут головокружительные склоны, бодрящий морозный воздух и новые трассы для катания на лыжах и сноуборде. Здесь, у подножья гор Заилийского Алатау, вы также найдете пешие тропы для любителей длительных прогулок, отдельные дорожки для велосипедистов и мотоциклистов, для удобства передвижения и наслаждения шикарными видами &ndash; канатную дорогу, уютнейшие финские беседки с пылающим камином и песнями под гитару и многое-многое другое.</li>
                            <li>Здесь вам рады круглый год, ведь уникальные пейзажи Алматинской области любому вскружат голову: &nbsp;зимой &ndash; белоснежными покровами и склонами, которые так и манят всех любителей активного отдыха; весной &ndash; невероятными красками пробуждающейся природы ; летом &ndash; яркой зеленью и ароматами разнотравья;&nbsp; осенью &ndash; ароматными яблоками и золотыми листопадами.</li>
                            <li>Обновленный спортивно-развлекательный комплекс &nbsp;&laquo;<strong>Табаган</strong>&raquo; рад представить новую парковую зону <strong>YetiPark</strong>. Что же это такое:
                                <ul>
                                    <li>загадочная тематика и дизайн, посвященные самому таинственному существу, которое, согласно легенде, обитает среди неприступных заснеженных горных вершин;</li>
                                    <li>3 новых лыжных склона (экстрим трасса, трассы для лыжников и сноубордистов);</li>
                                    <li>5-ти полосная 300 метровая горка: удобная, ровная, скоростная и безопасная, с отдельно выделенными полосами;</li>
                                    <li>футдартс:  игра для самых активных: вам предстоит отточить свои снайперские качества, стараясь пнуть мяч так, чтобы тот попал ближе к центру мишени;</li>
                                    <li>финские беседки, где вы можете согреться и отведать вкуснейшую мраморную говядину  или другие блюда по меню либо приготовить их на гриле самостоятельно, насладиться согревающим глинтвейном.</li>
                                </ul>
                            </li>
                            <li>Номера спортивно-развлекательного комплекса &laquo;<strong>Табаган</strong>&raquo; порадуют вас безупречным сервисом Room Service и горячими завтраками. Сами номера оборудованы всем необходимым: горячая и холодная вода, центральное отопление, возможность организовать дополнительные места в номере.</li>
                            <li><a href="https://chocolife.me/static/upload/files/deal/main/40000/39540/%D1%81%D1%85%D0%B5%D0%BC%D1%83%20%D1%81%D0%BA%D0%BB%D0%BE%D0%BD%D0%B0.pdf?42373" target="_blank">Схема склона</a><br />
Протяженность бэби-лифта: 200 м.&nbsp;<br />
Протяженность трасс: до 4 км. (экстрим-трасса и трассы для катания на лыжах и сноуборде).&nbsp;</li>
                            <li>Сайт партнера:                                                            <a data-seohide-href="/deal/away/39540/?key=0"
                                   class="e-offer__feature--link seohide-link"
                                   target="_blank"
                                   rel="nofollow"
                                   title="http://www.tabagan.com">
                                    www.tabagan.com                                </a><br></li>
                            <li>Скачать                                    <a href="https://chocolife.me/static/upload/files/deal/main/40000/39540/прайс-лист услуг.pdf"
                                       target="_blank"
                                       title="">
                                        прайс-лист услуг                                    </a></li>
                            <li>Скачать                                    <a href="https://chocolife.me/static/upload/files/deal/main/40000/39540/схему склона.pdf"
                                       target="_blank"
                                       title="">
                                        схему склона                                    </a></li>
                            <p style="margin-bottom: 17px;font-weight: 700;line-height: 17px;">СРК Табаган в социальных сетях:</p>
                            <p class="links2"><a href=""><img src="images/vk.png" alt=""></a>
                            <a href=""><img src="images/face.png" alt=""></a></p>
                            

                        </ul>
                    </div>
                </div>

            </div>
            <div class="row bot-line">
                <div class="col-xs-2"><p class="top-text2">Способы оплаты:</p></div>
                <div class="col-xs-3"><span style="position: relative;bottom: 10px;right: 50px"><img src="images/visa.png" alt=""></span></div>
                <div class="col-xs-4"><a href="{{route('crud.index')}}" class="buy">Есть вопросы?</a></div>
                <div class="col-xs-3"><span class="buy"><a href="">Купить</a></span></div>
                
            </div>
        </div>
    </div>


	


	<div class="clear3"></div>
	<!-- footer -->
    <div class="container-fluid fluid2">
    	<div class="container">
    		<div class="row" >
    			<div class="col-xs-2">
    				<p class="inner-h3"> Компания </p>
                    <ul class="link-list">
                        <li> <a href="#"> О Chocolife.me </a> </li>
                        <li> <a href="#"> Пресса о нас</a> </li>
                        <li> <a href="#"> Контакты </a> </li>
                    </ul>
    			</div>
    			<div class="col-xs-2">
    				<p class="inner-h3"> Клиентам </p>
                    <ul class="link-list">
                        <li> <a href="#"> Обратная связь </a> </li>
                        <li> <a href="#"> Обучающий видеоролик</a> </li>
                        <li> <a href="#"> Вопросы и ответы </a> </li>
                        <li> <a href="#"> Публичная оферта </a> </li>
                    </ul>
    			</div>
    			<div class="col-xs-2">
    				<p class="inner-h3"> Партнерам </p>
                    <ul class="link-list">
                        <li> <a href="#"> Для Вашего Бизнеса </a> </li>
  
                    </ul>
    			</div>
    			<div class="col-xs-2"></div>
    			<div class="col-xs-4" style="padding-left: 50px;">
    				<p class="inner-h4"> Наше предложение </p>
                    <ul class="link-list" >
                        <li style="padding-bottom: 10px;"> Chocolife.me теперь еще удобнее и <br>всегда под рукой! <br>
  						<a href=""><img src="images/gplay.png" alt=""></a>
  						<a href=""><img src="images/app.png" alt=""></a>
  						</li>
                    </ul>
    			</div>
    		</div>
			<div class="clear4"></div>
    		<div class="row" style="padding-top: 15px">
    			<div class="col-xs-7">
    				<ul class="link-list">
                        <li> Chocolife.me | 2011-2018    -    Карта сайта</li>
  					
                    </ul>
    			</div>
    			<div class="col-xs-5 text-right" style="padding-left: 50px;">
    				<ul class="link-list">
                        <li> Chocolife.me в социальных сетях:
  						<a href=""><img src="images/vk.png" alt=""></a>
  						<a href=""><img src="images/face.png" alt=""></a>
  						<a href=""><img src="images/you.png" alt=""></a>
  						<a href=""><img src="images/insta.png" alt=""></a></li>
                    </ul>
    			</div>
    		</div>
    	</div>
    </div>  
	
	
</body>
</html>