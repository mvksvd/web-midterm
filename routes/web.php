<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('hello/{name}', function ($name) {
    echo 'Hello ' . $name;
});
Route::get('/', function(){return view('index');});
Route::get('/cirk', function(){return view('cirk');});
Route::get('/tabagan', function(){return view('tabagan');});


Route::get('/crud', 'QuestController@index')->name('crud.index');


Route::get('/crud/create', 'QuestController@create')->name('crud.create');

Route::post('/crud/store', 'QuestController@store')->name('crud.store');

Route::get('/crud/{id}/show', 'QuestController@show')->name('crud.show');

//Route::get('crud/{id}/edit', 'CrudController@edit')->name('crud.edit');

//Route::post('crud/{id}/update', 'CrudController@update')->name('crud.update');

//Route::delete('crud/{id}/destroy', 'CrudController@destroy')->name('crud.destroy');