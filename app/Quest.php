<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quest extends Model
{
    protected $table = 'quest';
    protected $fillable = ['title','description'];
}
