<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Crud;
use App\Http\Requests\createCrudRequest;
use Illuminate\Foundation\Validation\ValidatesRequests;



class CrudController extends Controller
{
	

    public function index(){
    	$crud=Crud::all();
    	
		return view('crud.index', compact('crud'));

    }
    public function create(){
		return view('crud.create');

    }
    public function store(CreateCrudRequest $request){


    	Crud::create($request->all());

		return redirect()->route('crud.index');

		
	}
	public function edit($id){

		$myTask=Crud::find($id);

		return view('crud.edit', compact('myTask','id'));

	}
	public function update(Request $req, $id){

		Crud::find($id)->update($req->all());

		return redirect()->route('crud.index');
	}

	public function destroy($id){
		Crud::find($id)->delete();
		return redirect()->route('crud.index');
	}


}

