<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quest;
use App\Http\Requests\createQuestRequest;
use Illuminate\Foundation\Validation\ValidatesRequests;


class QuestController extends Controller
{
    public function index(Request $request){
    	//$quest=Quest::all();
    	$quest=Quest::orderBy('id','ASC')->paginate(5);
    	
		return view('crud.index', compact('quest'))->with('i',($request->input('page',1)-1)*5);

    }
    public function create(){
		return view('crud.create');

    }
    public function store(CreateQuestRequest $request){


    	Quest::create($request->all());

		return redirect()->route('crud.index');

		
	}
	public function show($id){

		$myTask=Quest::find($id);

		
		return view('crud.show', ['task'=>$myTask]);

	}
}
